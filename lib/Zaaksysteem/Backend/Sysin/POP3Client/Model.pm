package Zaaksysteem::Backend::Sysin::POP3Client::Model;
use Moose;
use namespace::autoclean;

with 'MooseX::Log::Log4perl';

use Net::POP3;
use BTTW::Tools;

=head1 NAME

Zaaksysteem::Backend::Sysin::POP3Client::Model - Model for email intake (pop3 bits)

=head1 ATTRIBUTES

These attributes are based on the configuration of the EmailIntake interface,
which is passed to the constructor by C<_get_model>.

=head2 protocol

Currently, the only allowed value is 'POP3' (which means 'POP3 + STLS').

=cut

has protocol => (
    is       => 'ro',
    required => 0,
);

=head2 host

The hostname of the mail server (pop3.example.com)

=cut

has host => (
    is       => 'ro',
    required => 0,
    default  => '',
);

=head2 port

The port the mail server can be contacted on (by default, 110)

=cut

has port => (
    is       => 'ro',
    required => 0,
    default  => 110,
);

=head2 user

Username to use to log in on the POP3 server.

=cut

has user => (
    is       => 'ro',
    required => 0,
    default  => '',
);

=head2 password

Password to use to log in on the POP3 server.

=cut

has password => (
    is       => 'ro',
    required => 0,
    default  => '',
);

=head2 pop3

A C<Net::POP3> instance to retrieve email from.

=cut

has pop3 => (
    is => 'ro',
    required => 0,
    lazy => 1,
    builder => '_pop3_builder',
    clearer => '_clear_pop3',
);

sub _pop3_builder {
    my $self = shift;

    my %extra;
    if ($self->protocol eq 'POP3S') {
        $extra{SSL}         = 1;
        $extra{SSL_ca_path} = '/etc/ssl/certs';
    }

    return Net::POP3->new(
        Host    => $self->host,
        Port    => $self->port,
        Timeout => 60,
        %extra,
    );
}

=head1 METHODS

=head2 login

Log in on the POP3 server using the configured credentials.

=cut

sub login {
    my $self = shift;

    if ($self->protocol eq 'POP3') {
        $self->pop3->starttls(SSL_ca_path => '/etc/ssl/certs')
            or throw('pop3/starttls_failed', 'Failed to secure POP3 connection using STLS');
    }

    $self->pop3->login($self->user, $self->password)
        or throw('pop3/login_failed', "POP3 Login failed");

    return;
}

=head2 messages

Check for incoming mail on the POP3 server. Returns a generator that can be
used to retrieve messages using C<< $gen->next >>.

=cut

sub messages {
    my ($self) = @_;

    my $pop = $self->pop3;

    my $messages = $pop->list
        or throw(
            "pop3/list_failed",
            sprintf(
                "Failed to retrieve list of messages from POP3 server '%s'",
                $self->host,
            ),
        );

    my @messages = keys %$messages;

    return sub {
        return unless @messages;

        Zaaksysteem::Backend::Sysin::POP3Client::Model::Message->new(
            pop3   => $pop,
            msgnum => pop @messages,
        );
    };
}

sub close {
    my $self = shift;

    $self->pop3->quit();
    $self->_clear_pop3();
}

__PACKAGE__->meta->make_immutable();

package Zaaksysteem::Backend::Sysin::POP3Client::Model::Message {
    use Moose;
    use namespace::autoclean;
    with 'MooseX::Log::Log4perl';

    has pop3 => (is => 'ro');
    has msgnum => (is => 'ro', isa => 'Str');

    sub retrieve {
        my $self = shift;

        $self->log->trace("Retrieving message " . $self->msgnum);

        my $message;
        unless ($message = $self->pop3->get($self->msgnum)) {
            $self->log->warn("POP3: Failed to fetch message " . $self->msgnum . ": " . $self->pop3->message);
            return;
        }

        return join("", @$message);
    }

    sub delete {
        my $self = shift;

        $self->log->trace("Removing message " . $self->msgnum);

        $self->pop3->delete($self->msgnum)
            or $self->log->warn("POP3: Failed to delete message ". $self->msgnum);

        return;
    }

    __PACKAGE__->meta->make_immutable();
};

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
