=head1 NAME

Zaaksysteem::Manual::API::V1::Types::Filebag - A Filebag object

=head1 Description

This API-document describes the usage of the L<Zaaksysteem::API::v1::PreparedFileBag> object

The Filebag object is not a real object within Zaaksysteem, it is an
object to return the status of files which are uploaded to Zaaksysteem.

As you cannot upload files to cases directly, you must use the
L<prepare_file|Zaaksysteem::Manual::API::V1::Case#prepare_file> call to upload files.

The object exposes some information useful to the uploader, such as the
UUID of the file and the original filename in the reference.

=head2 references

The references dictrionairy tells you which file you uploaded has which
UUID in Zaaksysteem, which you can then use to update attributes in the
case.


=begin javascript

    {
      "api_version": 1,
      "development": true,
      "request_id": "vagrant-316afb-2ca1e6",
      "result": {
        "instance": {
          "references": {
            "54fe646f-e9a4-4f9e-bc91-6a1fb3bdd735": "DANSpreferredformatsUK-21_1.png"
          }
        },
        "reference": null,
        "type": "filebag"
      },
      "status_code": 200
    }

=cut

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
