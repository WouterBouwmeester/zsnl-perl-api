use utf8;
package Zaaksysteem::Schema::CaseV0;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Zaaksysteem::Schema::CaseV0

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<Zaaksysteem::Result>

=cut

use base 'Zaaksysteem::Result';
__PACKAGE__->table_class("DBIx::Class::ResultSource::View");

=head1 TABLE: C<case_v0>

=cut

__PACKAGE__->table("case_v0");
__PACKAGE__->result_source_instance->view_definition(" SELECT z.uuid AS id,\n    z.id AS object_id,\n    build_case_v0_values(hstore(z.*), hstore(zm.*), hstore(zt.*), hstore(ztr.*), hstore(zts.*), hstore(zts_next.*), hstore(rpt.*), hstore(ztd.*), hstore(gr.*), hstore(ro.*), hstore(case_location.*), hstore(parent.*), hstore(requestor.*), hstore(recipient.*), (bc.naam)::text, gassign.name, ztn.v0_json) AS \"values\",\n    build_case_v0_case(hstore(zt.*), hstore(zm.*)) AS \"case\",\n    '{}'::text[] AS related_objects,\n    'case'::text AS object_type\n   FROM (((((((((((((((((zaak z\n     JOIN zaak_meta zm ON ((z.id = zm.zaak_id)))\n     JOIN zaaktype zt ON ((z.zaaktype_id = zt.id)))\n     JOIN zaaktype_node ztn ON ((z.zaaktype_node_id = ztn.id)))\n     LEFT JOIN zaaktype_resultaten ztr ON ((z.resultaat_id = ztr.id)))\n     LEFT JOIN zaaktype_status zts ON (((z.zaaktype_node_id = zts.zaaktype_node_id) AND (zts.status = z.milestone))))\n     LEFT JOIN zaaktype_status zts_next ON (((z.zaaktype_node_id = zts_next.zaaktype_node_id) AND (zts_next.status = (z.milestone + 1)))))\n     LEFT JOIN result_preservation_terms rpt ON ((ztr.bewaartermijn = rpt.code)))\n     LEFT JOIN bibliotheek_categorie bc ON ((zt.bibliotheek_categorie_id = bc.id)))\n     LEFT JOIN zaaktype_definitie ztd ON ((ztn.zaaktype_definitie_id = ztd.id)))\n     LEFT JOIN groups gr ON ((z.route_ou = gr.id)))\n     LEFT JOIN roles ro ON ((z.route_role = ro.id)))\n     LEFT JOIN zaak_bag case_location ON (((case_location.id = z.locatie_zaak) AND (z.id = case_location.zaak_id))))\n     LEFT JOIN subject assignee ON ((z.behandelaar_gm_id = assignee.id)))\n     LEFT JOIN groups gassign ON ((assignee.group_ids[1] = gassign.id)))\n     LEFT JOIN zaak parent ON ((z.pid = parent.id)))\n     LEFT JOIN zaak_betrokkenen requestor ON (((z.aanvrager = requestor.id) AND (z.id = requestor.zaak_id) AND (requestor.deleted IS NULL))))\n     LEFT JOIN zaak_betrokkenen recipient ON ((recipient.id = ( SELECT zb.id\n           FROM zaak_betrokkenen zb\n          WHERE ((z.id = zb.zaak_id) AND (zb.rol = 'Ontvanger'::text) AND (zb.deleted IS NULL))\n          ORDER BY zb.id\n         LIMIT 1))))\n  WHERE (z.deleted IS NULL)");

=head1 ACCESSORS

=head2 id

  data_type: 'uuid'
  is_nullable: 1
  size: 16

=head2 object_id

  data_type: 'integer'
  is_nullable: 1

=head2 values

  data_type: 'jsonb'
  is_nullable: 1

=head2 case

  data_type: 'jsonb'
  is_nullable: 1

=head2 related_objects

  data_type: 'text[]'
  is_nullable: 1

=head2 object_type

  data_type: 'text'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "uuid", is_nullable => 1, size => 16 },
  "object_id",
  { data_type => "integer", is_nullable => 1 },
  "values",
  { data_type => "jsonb", is_nullable => 1 },
  "case",
  { data_type => "jsonb", is_nullable => 1 },
  "related_objects",
  { data_type => "text[]", is_nullable => 1 },
  "object_type",
  { data_type => "text", is_nullable => 1 },
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2022-06-17 13:42:17
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:gwGeIFEDcVJs49fK/cue6g


# You can replace this text with custom code or comments, and it will be preserved on regeneration
require JSON::XS;


my @hash_fields = qw(
  case
  values
);

foreach (@hash_fields) {
  __PACKAGE__->inflate_column($_, {
    inflate => sub { JSON::XS->new->decode(shift // '{}') },
  });
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2021, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
