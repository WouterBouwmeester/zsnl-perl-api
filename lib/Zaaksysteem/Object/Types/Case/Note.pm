package Zaaksysteem::Object::Types::Case::Note;
use Moose;
use namespace::autoclean;

extends 'Zaaksysteem::Object';

with 'Zaaksysteem::Object::Roles::Security';

use Zaaksysteem::Types qw(UUID);

=head1 NAME

Zaaksysteem::Object::Types::Case::Note - User-specific notes on cases

=head1 DESCRIPTION

=head1 ATTRIBUTES

=head2 content

Content of the note

=cut

has content => (
    is       => 'rw',
    isa      => 'Str',
    label    => 'Naam',
    traits   => [qw[OA]],
    required => 1
);

=head2 owner

The creator/owner of this comment

=cut

has owner_id => (
    is       => 'rw',
    isa      => UUID,
    label    => 'Eigenaar',
    traits   => [qw(OA)],
    required => 1,
);

=head2 case

The case this note applies to.

=cut

has case => (
    is       => 'rw',
    type     => 'case',
    label    => 'Zaak',
    traits   => [qw(OR)],
    embed    => 0,
);

=head1 METHODS

=head2 type

Overrides L<Zaaksysteem::Object/type>, always returns the string C<case/note>.

=cut

override type => sub { 'case/note' };

=head2 TO_STRING

Overrides L<Zaaksysteem::Object/TO_STRING>, returns the L</content> of the
note.

=cut

override TO_STRING => sub {
    return shift->content;
};

__PACKAGE__->meta->make_immutable

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
