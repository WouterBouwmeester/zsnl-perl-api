package Zaaksysteem::ZTT::JSONPath;
use Zaaksysteem::Moose;

with 'Zaaksysteem::Moose::Role::Schema';

use JSON::Path;
use JSON::XS qw(decode_json encode_json);
use List::Util qw(none);
use Scalar::Util qw(blessed);
use Encode qw(encode_utf8);

has '+schema' => (
    required => 0,
    writer   => '_schema',
    init_arg => undef,
);

has context => (
    isa       => 'Defined',
    predicate => 'has_context',
    writer    => '_context',
    init_arg => undef,
);

has custom_object_version_rs => (
    is => 'ro',
    isa => 'Defined',
    lazy => 1,
    builder => '_build_custom_object_version',
    init_arg => undef,
);

has case => (
    is        => 'ro',
    isa       => 'Zaaksysteem::Model::DB::Zaak',
    writer    => '_case',
    init_arg  => undef,
    predicate => 'has_case',
);

sub _build_custom_object_version {
    my $self = shift;
    return $self->build_resultset(
        'CustomObjectVersion',
        undef,
        {
            join     => 'custom_object_id',
            prefetch => 'custom_object_version_content_id',
        },
    );
}

sub add_context {
    my ($self, $context) = @_;

    if (blessed $context && $context->isa('Zaaksysteem::Model::DB::Zaak')) {
        my $values = $context->field_values(
            {
                use_magic_string_keys => 1,
                apply_field_filters   => 1
            }
        );
        $self->_context($values);
        $self->_schema($context->result_source->schema);
        $self->_case($context);
        return 1;
    }
    return 0;

}

sub _resolve_simple_value {
    my ($self, $value) = @_;

    my $ref = ref $value;

    if (not $ref) {
        # Regular single-value field
        return $value;
    } elsif ($ref eq 'ARRAY')  {
        # Regular multivalue field
        return join(
            " ",
            map {
                $self->_resolve_simple_value($_)
            } @$value
        );
    } elsif ($ref eq 'HASH') {
        if (($value->{type} //'') eq 'relationship') {
            # Multi-value "relationship" field
            if ((ref($value->{value}) // '') eq 'ARRAY') {
                return join(
                    " ",
                    map {
                        $_->{specifics}{metadata}{summary}
                    } @{ $value->{value} }
                );

            }
            # Single-value "relationship" field
            return $value->{specifics}{metadata}{summary};
        }
        elsif (exists $value->{label}) {
            return $value->{label};
        }
    }

    return 'Unsupported value, unable to serialize';
}

sub resolve_jpath {
    my ($self, $path) = @_;

    return '' unless $self->has_context;

    # $.attribute.custom_fields.attr.what
    # or
    # $.relationships.type
    # but prolly more to add after .type in a later stage
    my (undef, @path) = split(/\./, $path);

    if ($path[0] eq 'attributes' && $path[1] eq 'custom_fields') {
        return $self->_process_custom_fields(@path[2 .. $#path]);
    }

    if ($path[0] eq 'relationships') {
        return $self->_process_case_relationships(@path[1 .. $#path]);
    }

    return "Unsupported path $path";
}


sub _process_case_relationships {
    my ($self, @path) = @_;

    # Unsupported path used
    return '' if @path != 1;

    # No case, nothing to lookup
    return '' if !$self->has_case;

    # We only support these four types
    if (none { $path[0] eq $_ } qw(requestor recipient assignee coordinator)) {
        return '';
    }
    return $self->case->load_v2_relationship_role($path[0]);

}

sub _process_custom_fields {
    my ($self, $magic_string, @path) = @_;

    my $value = $self->context->{$magic_string};
    return '' unless defined $value;

    my $what = $path[0];
    return $self->_resolve_simple_value($value) if $what eq 'value';

    return $self->_resolve_relationship_objects($value, join(".", @path));
}

my $object_path = qr/^attributes\.custom_fields\./;

sub _resolve_relationship_objects {
    my ($self, $object, $jpath) = @_;

    return '' unless $jpath =~ m/$object_path/;

    $jpath =~ s/$object_path/\$./;

    my $ref = ref $object;
    if ($ref ne 'HASH') {
        return 'Unsupported value, unable to serialize';
    }

    if ($object->{type} eq 'custom_object') {
        return $self->_resolve_custom_object($object->{id}, $jpath);
    }
    elsif ($object->{type} eq 'relationship' && $object->{specifics}{relationship_type} eq 'custom_object') {
        return $self->_resolve_custom_object($object->{value}, $jpath);
    }

    return '';
}

sub _find_custom_object_version {
    my ($self, $object_id) = @_;

    return $self->custom_object_version_rs->search_rs(
        { 'custom_object_id.uuid' => $object_id },
        {
            order_by => { '-desc' => 'version' },
            rows     => 1,
        },
    )->single;
}

sub _resolve_custom_object {
    my ($self, $object_id, $path) = @_;

    my $version_object = $self->_find_custom_object_version($object_id);
    return '' unless $version_object;

    my $co_content = $version_object->custom_object_version_content_id;
    return '' unless $co_content;

    my $data = $co_content->custom_fields;
    return '' unless $data;

    try {
        my $jpath = JSON::Path->new($path);
        return $jpath->value(decode_json(encode_utf8($data)));
    }
    catch {
        $self->log->info(
            sprintf("Error while decoding %s: %s", dump_terse($data), $_));
        return '';
    }
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 DESCRIPTION

=head1 SYNOPSIS

    use Zaaksysteem::ZTT::JSONPath;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2021, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.

