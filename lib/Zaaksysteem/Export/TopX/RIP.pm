package Zaaksysteem::Export::TopX::RIP;
use Zaaksysteem::Moose;

=head1 NAME

Zaaksysteem::Export::TopX::RIP - Export cases in TopX/RIP format

=head1 DESCRIPTION

This model hides the logic for developers to transform data to CSV.

=head1 SYNOPSIS

    use Zaaksysteem::Export::TopX::RIP;

    my $model = Zaaksysteem::Export::TopX::RIP->new(
      schema  => $self->schema,
      user    => $user,
      objects => $results,
      tar     => $tar,
      $mapping ? (attribute_mapping => $mapping) : (),
    );

    $model->export();

=cut

with 'Zaaksysteem::Export::TopX::Base';

use DateTime;
use BTTW::Tools::RandomData qw(generate_uuid_v4);
use XML::LibXML;
use Zaaksysteem::XML::Compile;

has '+identification' => (required => 1);

has rip => (
    is => 'ro',
    isa => 'Defined',
    lazy => 1,
    builder => '_build_rip',
);

has target_system => (
    is => 'ro',
    isa => 'Str',
    predicate => 'has_target_system',
);

sub _build_rip {
    return Zaaksysteem::XML::Compile->xml_compile->add_class(
        'Zaaksysteem::XML::RIP::Instance'
    )->rip;
}

has records => (
    is      => 'ro',
    isa     => 'ArrayRef',
    default => sub { [] },
    traits  => [qw(Array)],
    handles => {
        add_record  => 'push',
        has_records => 'count',
    },
);

sub _export_case_xml {
    my ($self, $case, $xml) = @_;

    my $record = $self->_xml_to_record($case->get_column('uuid'), $xml);
    $self->add_record($record);

    $self->add_case_record($case);

    return 1;
}

sub add_case_record {
    my ($self, $case) = @_;

    my $id     = $case->alternative_case_id;
    my $xml    = $self->_create_case_record_xml($id, $case->get_column('uuid'));
    my $record = $self->_xml_to_record($id, $xml);
    $self->add_record($record);
    return 1;
}

sub _export_case_document_xml {
    my ($self, $case, $file, $xml) = @_;

    my $record = $self->_xml_to_record($file->get_column('uuid'), $xml);
    $self->add_record($record);

    my $uuid = $file->uuid;
    my $name = sprintf("%s.blob", $file->uuid);
    $self->add_fh_to_export($name, $file->filestore->get_path);
    return 1;
}

sub _xml_to_record {
    my ($self, $id, $xml) = @_;

    return {
        recordHeader => {
            identificatie => $id,
            status        => 'nieuw',
        },
        metadata => $self->_xml_to_metadata($xml),
    };
}

sub _xml_to_metadata {
    my ($self, $xml) = @_;

    my $dom = XML::LibXML->load_xml(string => $xml);

    my $root      = $dom->documentElement();
    my $localname = $root->localname;
    my $ns        = $root->namespaceURI();

    my %metadata = (
        schemaURI         => $ns,
        "{$ns}$localname" => $root,
    );
    return \%metadata;
}

sub add_archive_xml {
    my ($self) = @_;

    my $xml = $self->_create_archive_xml;
    $self->add_record($self->_xml_to_record($self->identification, $xml));
    return 1;
}

sub _create_rip_package {
    my $self = shift;

    my $config = $self->customer_config;

    $self->add_archive_xml;

    my %data = (
        packageHeader => {
            identificatie => $self->identification,
            datum         => DateTime->now->ymd,
            bron          => $config->{naam_lang},
            $self->has_target_system ? (doel => $self->target_system) : (),
        },
        record => $self->records,
    );

    my $xml = $self->rip->tmlo_export('writer', \%data);
    $self->add_scalar_to_export('export.xml', $xml);
    return 1;

}

after export => sub {
    my $self = shift;
    return $self->_create_rip_package if $self->has_records;
};

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2021, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
