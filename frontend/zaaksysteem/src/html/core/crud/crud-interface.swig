<div data-ng-controller="nl.mintlab.core.crud.CrudInterfaceController" class="crud" data-ng-class="{ 'crud-loading': isLoading(), 'crud-column-managed': hasColumnManagement(), 'crud-has-loaded': hasLoaded(), 'crud-single-select': options.select === 'single' }">
	
	<div class="crud-pagination crud-pagination-top clearfix" data-zs-pagination data-zs-pagination-current="<[currentPage]>" data-zs-pagination-total="<[numPages]>" data-zs-pagination-num-rows="<[perPage]>" data-ng-show="getItems()" data-ng-if="showPagination()">
	</div>
	
	<div class="crud-filters" data-ng-show="filters.length" data-zs-form-template-parser="<[getFormConfig()]>">
		
	</div>
	
	<div class="crud-wrapper">
	
		<div class="crud-loader">
		</div>
		
		<div class="crud-table-wrapper">

			<div data-ng-controller="nl.mintlab.core.crud.CrudTableController" class="table crud-table">

				<div class="table-header crud-header" data-ng-class="{'crud-hidden': hasVisibleActions()}" data-ng-controller="nl.mintlab.core.crud.CrudColumnManagementController">
					<div class="table-row crud-table-head-row" data-zs-sort data-zs-sort-direction="horizontal">

						<div class="table-cell table-cell-header crud-table-select-cell" data-ng-show="options.select==='all'">
							<input type="checkbox" data-ng-click="handleSelectAllClick(e$vent)" data-ng-checked="isAllSelected(item)" data-zs-title="Alles selecteren"/>
						</div>

						<div class="table-cell table-cell-header crud-table-column-header crud-table-column-header-<[getCssColumnId(column)]>" data-ng-repeat="column in columns track by column.id" data-ng-draggable="hasColumnManagement()&&!column.locked" data-ng-drag-mimetype="zs/column" data-zs-sortable="column" data-ng-class="{'crud-table-column-header-locked': column.locked }">
							<div class="crud-table-column-header-inner">
								<div class="crud-table-column-header-label-button" data-ng-click="sortOn(column)" title="<[column.label]>">
									<span class="column-label"><[column.label]></span>
									<span class="column-sort-asc column-sort" data-ng-show="isSortedOn(column)&&!isReversed()">&#9650;</span>
									<span class="column-sort-desc column-sort" data-ng-show="isSortedOn(column)&&isReversed()">&#9660;</span>
								</div>
								<div class="crud-table-column-header-remove" data-ng-if="hasColumnManagement()&&!column.locked">
									<button class="crud-table-column-header-remove-button" data-ng-click="removeColumn(column)">
										<i class="mdi mdi-close"></i>
									</button>
								</div>
							</div>
						</div>
						
					</div>
				</div>

				<div data-ng-show="hasVisibleActions()" class="table-header crud-actions">
					<div class="table-row crud-table-head-row">
						<div class="crud-table-select-cell" data-ng-show="options.select==='all'">
							<input type="checkbox" data-ng-click="handleSelectAllClick($event)" data-ng-checked="isAllSelected()" data-ng-checked="options.select==='all'"/>
						</div>
						<div class="crud-table-action-cell">
							<ul data-ng-include="'/html/core/crud/action-menu.html'">
							</ul>
						</div>
					</div>
				</div>
				
				<div data-ng-if="(isAllSelected() && numPages > 1)" class="table-header crud-select-all-wrapper">
					<div class="table-row crud-table-head-row">
						<div>
							<div class="crud-select-all">
								<div data-ng-show="zsCrud.getSelectionType()=='subset'">Alleen de objecten op deze pagina zijn geselecteerd. <button class="crud-select-all-button" data-ng-click="setSelectionType('all')">Selecteer alle <[numRows]> objecten.</button></div>
								<div data-ng-show="zsCrud.getSelectionType()=='all'">Alle <[numRows]> objecten zijn geselecteerd. <button class="crud-select-all-button" data-ng-click="setSelectionType('subset')">Selecteer alleen de objecten op deze pagina.</button></div>
							</div>
						</div>
					</div>
				</div>

				<div class="table-body" data-zs-selectable-list data-zs-selectable-list-mode="<[options.select]>">
					<div class="table-row crud-table-item-row crud-table-item-row-empty no-results" data-ng-show="isEmptyResultSet()&&hasLoaded()">
						Geen resultaten	
					</div>
					
					<a class="table-row crud-table-item-row <[isSelected(item) && 'crud-table-item-row-selected'||'']>" data-ng-class="getItemStyle(item)" data-ng-repeat="item in getItems() track by zsCrud.getTrackingId(item, $index)" data-zs-selectable data-zs-select-data="item" data-zs-contextmenu="hasVisibleActions()&&'/html/core/crud/context-menu.html'||''" data-ng-href="<[getUrl(item)]>" data-ng-include="'/html/core/crud/crud-item-row.html'" data-ng-if="getUrl(item)">
						
					</a>
					<div class="table-row crud-table-item-row <[isSelected(item) && 'crud-table-item-row-selected'||'']>" data-ng-class="getItemStyle(item)" data-ng-repeat="item in getItems() track by zsCrud.getTrackingId(item, $index)" data-zs-selectable data-zs-select-data="item" data-zs-contextmenu="hasVisibleActions()&&'/html/core/crud/context-menu.html'||''" data-ng-if="!getUrl(item)" data-ng-include="'/html/core/crud/crud-item-row.html'"
						
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="crud-pagination crud-pagination-bottom clearfix" data-zs-pagination data-zs-pagination-current="<[currentPage]>" data-zs-pagination-total="<[numPages]>" data-zs-pagination-loading="<[loading]>" data-zs-pagination-num-rows="<[perPage]>" data-ng-show="getItems().length" data-ng-if="showPagination()">
	</div>

</div>

<script type="text/ng-template" id="/html/core/crud/crud-item-row.html">
	<div class="table-cell crud-table-item-cell crud-table-select-cell" data-ng-show="options.select==='all'">
		<input type="checkbox" data-ng-checked="isSelected(item)"/>
	</div>
	<div class="table-cell crud-table-item-cell crud-table-value-cell crud-table-value-cell-<[getCssColumnId(column)]>" data-ng-repeat="column in columns track by column.id" data-zs-crud-value-cell>
	</div>
</script>

<script type="text/ng-template" id="/html/core/crud/action-menu.html">
	<li class="crud-action" data-ng-repeat="action in actions | filter:isVisible:action track by action.id " data-ng-include="'/html/core/crud/action-type-' + action.type + '.html'" data-ng-controller="nl.mintlab.core.crud.CrudActionController">
	</li>
	<li class="crud-action crud-action-empty" data-ng-show="!hasVisibleActions(actions)">
		Geen acties beschikbaar
	</li>
</script>

<script type="text/ng-template" id="/html/core/crud/action-type-delete.html">
	<button type="button" data-ng-click="handleActionClick(action, $event)" class="button button-smaller button-secondary">
		<[action.label]>
	</button>
</script>

<script type="text/ng-template" id="/html/core/crud/action-type-update.html">
	<button type="button" data-ng-click="handleActionClick(action, $event)" class="button button-smaller button-secondary">
		<[action.label]>
	</button>
</script>

<script type="text/ng-template" id="/html/core/crud/action-type-download.html">
	<button type="button" data-ng-click="handleActionClick(action, $event)"  class="button button-smaller button-secondary">
		<[action.label]>
	</button>
</script>

<script type="text/ng-template" id="/html/core/crud/action-type-click.html">
	<button type="button" data-ng-click="handleActionClick(action, $event);closePopupMenu()"  class="button button-smaller button-secondary">
		<[action.label]>
	</button>
</script>

<script type="text/ng-template" id="/html/core/crud/action-type-url.html">
	<a data-ng-href="<[getActionUrl(action)]>" target="_blank" rel="noopener" class="button button-smaller button-secondary">
		<[action.label]>
	</a>
</script>

<script type="text/ng-template" id="/html/core/crud/action-type-popup.html">
	<button type="button" data-ng-click="handleActionClick(action, $event);openPopup();closePopupMenu()" data-zs-popup="action.data.url" class="button button-smaller button-secondary">
		<[action.label]>
	</button>
</script>

<script type="text/ng-template" id="/html/core/crud/action-type-confirm.html">
	<button type="button" data-ng-click="handleActionClick(action, $event)" data-zs-confirm="handleConfirmClick(action, $event);" data-zs-confirm-label="<[action.data.label]>" data-zs-confirm-verb="<[action.data.verb]>" data-zs-popup="action.data.url" class="button button-smaller button-secondary">
		<[action.label]>
	</button>
</script>

<script type="text/ng-template" id="/html/core/crud/action-type-ezra-dialog.html">
	<button type="button" class="button button-secondary button-smaller" data-zs-ezra-dialog="getEzraDialogParams()">
		<[action.label]>
	</button>
</script>

<script type="text/ng-template" id="/html/core/crud/action-type-dropdown.html">
	<div class="action-menu-dropdown" data-zs-popup-menu data-zs-popup-menu-behaviour="<[behaviour]>" data-zs-popup-menu-positioning="smart" data-ng-controller="nl.mintlab.core.crud.CrudActionDropDownController">
		<button class="action-menu-dropdown-button btn btn-secondary btn-dropdown btn-medium">
			<span class="action-menu-dropdown-button-inner">
				<span class="action-menu-dropdown-button-text"><[action.label]></span>
				<i class="mdi mdi-menu-down"></i>
			</span>
		</button>
		<ul class="action-menu-dropdown-list popup-menu" data-zs-popup-menu-list>
			<li class="action-menu-dropdown-list-item" data-ng-repeat="action in action.data.children | filter:isVisible:action track by $index" data-ng-include="'/html/core/crud/action-type-' + action.type + '.html'" data-ng-controller="nl.mintlab.core.crud.CrudActionController">
			</li>
			<li class="action-menu-dropdwn-list-item-none" data-ng-show="!hasVisibleActions(action.data.children)">
				Geen acties beschikbaar
			</li>
		</ul>
	</div>
</script>

<script type="text/ng-template" id="/html/core/crud/popup-modal.html">
	<div data-zs-modal data-zs-modal-title="<[action.data.title]>">
		<div data-ng-include="action.data.url">
		</div>
	</div>
</script>

<script type="text/ng-template" id="/html/core/crud/context-menu.html">
	<ul class="context-menu crud-context-menu" data-ng-include="'/html/core/crud/action-menu.html'">
	</ul>
</script>

<script type="text/ng-template" id="/html/core/crud/crud-item-action-menu.html">	
	<div class="crud-item-action-menu clearfix" data-zs-popup-menu data-zs-popup-menu-positioning="smart" data-ng-controller="nl.mintlab.core.crud.CrudItemActionMenuController">
		<button class="crud-item-action-menu-open-button btn btn-flat btn-small btn-round btn-secondary" data-zs-popup-menu-position-reference data-ng-click="handleOpenMenuClick($event)">
			<i class="mdi mdi-dots-vertical"></i>
		</button>
		<ul class="crud-item-action-menu-list popup-menu" data-ng-include="'/html/core/crud/action-menu.html'" data-zs-popup-menu-list data-ng-show="isPopupMenuOpen()">
		</ul>
	</div>
</script>
